use utf8;
package Hotels::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2014-12-21 15:46:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:D+asTT3GGMBm46ZtZrjYVA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
