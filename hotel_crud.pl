#!/usr/bin/env perl
use strict;
use warnings;
# use utf8;

use Dancer2;
use Plack::Builder;

use Data::UUID::MT;

use Data::Printer;

use Data::Section::Simple qw/get_data_section/;
my $vpath = get_data_section;

use FindBin;
use lib "$FindBin::Bin/lib";
use Hotels::Schema;

my $schema = Hotels::Schema->connect(
  'dbi:mysql:database=hotels',
  'root', 'hTc805_2NGs', { RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 1 }
);

# Developmant settings
set log => "core";
set warnings => 1;
set show_errors => 1;
set startup_info => 1;
set strict_config => 1;
set traces => 1;
set logger => 'File';

set engines => {
    template => {
      Haml => {
        cache => 0,
        path => $vpath,
      },
    },
};
set template => 'haml';

# Settings
#---------------------
set appname    => "Customers";
set charset    => "UTF-8";
#---------------------

hook 'before' => sub {
 headers 
    'Access-Control-Allow-Origin'   => 'http://localhost:5000/',
    'Access-Control-Allow-Methods'  => 'GET, PUT, POST, DELETE, OPTIONS',
    'Access-Control-Max-Age'        => 3600,
    'Access-Control-Allow-Headers'  => 'Content-Type, Authorization, X-Requested-With';
};

get '/' => sub { 
  template 'index';
};

=head1 TODO

+1) вывести на экран с базы
+2) реализовать сортировку
+3) реализовать глобальный поиск
+4) реализовать удаление строк (на клиенте и в базе)
+5) реализовать добавление строк (на клиенте и в базе)
+6) реализовать изменение строк (на клиенте и в базе)
+7) добавить Bootstrap 3 стили для форм
+8) заменить input.text на select для hotel_star
+9) заменить input.text на input.file
+10) сделать image preview
+11) реализовать file upload на сервере (генерация имени файла, сохранение файла на диск и запись пути в базу. Посмотреть как это делается в cpiera)
+12) реализовать toggle для image preview (показывать image preview, только когда выбран файл)
+13) реализовать вывод изображения в таблице
14) заменить названия для hotel star на звездочки
+15) поправить изменение (put) данных на клиенте и на сервере с учетом изображений
+16) поправить удаление (del) данных на клиенте и на сервере с учетом изображений

=cut

get '/hotels' => sub {
  content_type 'application/json';
  return to_json [$schema->resultset('Hotel')->search(undef, {result_class => 'DBIx::Class::ResultClass::HashRefInflator'})->all];
};

post '/hotels' => sub {
  # content_type 'application/json';
  my $file = upload('image');
  my ($file_extension) = $file->filename =~ m{\.([\w\d]*$)};
  my $filename = Data::UUID::MT->new()->create_string().".$file_extension";
  $file->copy_to(path(dirname(__FILE__),'public','images', $filename));

  my $result = $schema->resultset('Hotel')->create({
      hotel_name  =>  param('hotel_name'),
      hotel_star  =>  param('hotel_star'),
      country     =>  param('country'),
      city        =>  param('city'),
      address     =>  param('address'),
      phone       =>  param('phone'),
      image       =>  $filename,
    });
  # Записать данные в базу
  # debug '--- file: '.p($file);
  # my $result = $schema->resultset('Hotel')->create(from_json request->body); # TODO: реализовать обработку отсутствия данных для обязательных полей
  return to_json({status => (($result->in_storage) ? 'success' : 'failed')});
};

put '/hotels' => sub {
  # content_type 'application/json';
  my $file = upload('image');
  debug '--- file: '.p($file);
  my $filename;
  if ($file) {
    my ($file_extension) = $file->filename =~ m{\.([\w\d]*$)};
    $filename = Data::UUID::MT->new()->create_string().".$file_extension";
    $file->copy_to(path(dirname(__FILE__),'public','images', $filename));

  }

  my $result = $schema->resultset('Hotel')->update_or_create({
      hotel_id    => param('hotel_id'),
      hotel_name  =>  param('hotel_name'),
      hotel_star  =>  param('hotel_star'),
      country     =>  param('country'),
      city        =>  param('city'),
      address     =>  param('address'),
      phone       =>  param('phone'),
      image       =>  $filename,
    });

  # my $result = $schema->resultset('Hotel')->update_or_create(from_json request->body);
  return to_json({status => (($result->in_storage) ? 'success' : 'failed')});
};

del '/hotels/:hotel_id' => sub {
  content_type 'application/json';
  my $result = $schema->resultset('Hotel')->find(param('hotel_id'))->delete;
  return to_json({status => (($result->in_storage) ? 'failed' : 'success')});
};

builder {
    enable "StackTrace", force => 1;
    enable 'Debug', panels => [ qw<DBITrace Memory Timer> ];
    dance;
};
#dance;

__DATA__
@@ index.haml
!!!
%html
  %head
    %script{:src => "http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.js"}

    %link{:href => "http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css", :rel => "stylesheet"}

    %script{:src => "#{$request->{uri_base}}/Smart-Table/dist/smart-table.min.js"}

    :css
      .ng-table {
        border: 1px solid #000;
      }
      
    :javascript
      var app = angular.module('main',['smart-table']);
      app.directive('fileModel', ['$parse', function ($parse) {
          // TODO: реализовать image preview
          return {
              restrict: 'A',
              link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  
                  element.bind('change', function(){
                      scope.$apply(function(){
                          modelSetter(scope, element[0].files[0]);
                      });
                  });
              }
          };
      }]);
      app.service('saveItems', ['$http', function ($http) {
            this.saveItemToUrl = function(form_data_fields, url){
              var fd = new FormData();
              for (field in form_data_fields) {
                fd.append(field, form_data_fields[field]);
              }
              $http.post(url, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
                })
              .success(function(){
                })
              .error(function(){
                });
            }
          }]);
      app.service('editItems', ['$http', function ($http) {
            this.editItemToUrl = function(form_data_fields, url){
              var fd = new FormData();
              for (field in form_data_fields) {
                fd.append(field, form_data_fields[field]);
              }
              $http.put(url, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
                })
              .success(function(){
                })
              .error(function(){
                });
            }
          }]);
      app.controller('safeCtrl', ['$scope', '$http', 'saveItems', 'editItems', function($scope, $http, saveItems, editItems) {
            $scope.hotel_stars = ['Tourist', 'Standard', 'Comfort', 'First Class', 'Luxury'];

            $http.get('/hotels').success(function(response) {
                $scope.hotels = response;
              });

            // copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
            $scope.displayedHotels = [].concat($scope.hotels);

            // add to the real data holter
            // TODO: добавить проверку на пустоту в полях
            $scope.saveItem = function saveItem() {
              var hotel = {
                hotel_name  : $scope.hotel_name,
                hotel_star  : $scope.hotel_star,
                country     : $scope.country,
                city        : $scope.city,
                address     : $scope.address,
                phone       : $scope.phone,
                image       : $scope.myImage
              };

              if ($scope.change === 'Add new hotel') {
                //$http.post('/hotels', hotel); // TODO: реализовать hotels.push() если post.success, иначе выдать ошибку
                saveItems.saveItemToUrl(hotel, '/hotels');

            $http.get('/hotels').success(function(response) {
                $scope.hotels = response;
              });

            //    $scope.hotels.push(hotel);

              } else if ($scope.change === 'Save changes for hotel') {
                hotel.hotel_id = $scope.hotels[$scope.indx].hotel_id;
                // TODO: для saveItemToUrl добавить поле method (GET/POST/PUT)
                //$http.put('/hotels', hotel);
                editItems.editItemToUrl(hotel, '/hotels');

            $http.get('/hotels').success(function(response) {
                $scope.hotels = response;
              });

             /*   $scope.hotels[$scope.indx].hotel_name = $scope.hotel_name;
                $scope.hotels[$scope.indx].hotel_star = $scope.hotel_star;
                $scope.hotels[$scope.indx].country    = $scope.country   ;
                $scope.hotels[$scope.indx].city       = $scope.city      ;
                $scope.hotels[$scope.indx].address    = $scope.address   ;
                $scope.hotels[$scope.indx].phone      = $scope.phone     ;
                $scope.hotels[$scope.indx].myImage    = $scope.myImage   ;
                */

              }
              $scope.toggle = !$scope.toggle;
            };

            $scope.toggle = false;
            $scope.toggleAddItem = function() {
              $scope.change = 'Add new hotel';
              if (!$scope.toggle) { // ng-show == false
                $scope.toggle = true;
              }
            };

            //remove to the real data holder
            $scope.removeItem = function removeItem(hotel) {
              var index = $scope.hotels.indexOf(hotel);
              if (index !== -1) {
                $http.delete('/hotels/' + $scope.hotels[index].hotel_id);
                $scope.hotels.splice(index, 1);
              }
            };

            $scope.editItem = function editItem(hotel) {
              var index = $scope.hotels.indexOf(hotel);

              $scope.change = 'Save changes for hotel';

              if (!$scope.toggle) { // ng-show == false
                $scope.toggle = true;
              }

              $scope.hotel_name = $scope.hotels[index].hotel_name;
              $scope.hotel_star = $scope.hotels[index].hotel_star;
              $scope.country    = $scope.hotels[index].country   ;
              $scope.city       = $scope.hotels[index].city      ;
              $scope.address    = $scope.hotels[index].address   ;
              $scope.phone      = $scope.hotels[index].phone     ;
              $scope.myImage      = $scope.hotels[index].myImage     ;

              $scope.indx = index;
            }

          }]);
  %body{"ng-app" => "main"}
    %div{"ng-controller" => "safeCtrl"}
      %button.btn.btn-sm.btn-success{"ng-click" => "toggleAddItem()", :type => "button"}
        %i.glyphicon.glyphicon-plus
        Add hotel
      %div{"ng-show" => "toggle"}
        %form.form-horizontal
          %fieldset
            %legend Upload on form submit
            .form-group
              %label.control-label.col-xs-2{:for => "inputHotelName"} Hotel name
              .col-xs-5
                %input#inputHotelName.form-control{"ng-model" => "hotel_name", :placeholder => "Hotel name", :type => "text"}
            .form-group
              %label.control-label.col-xs-2{:for => "inputHotelStar"} Hotel star
              .col-xs-5
                %select#inputHotelStar.form-control{"ng-model" => "hotel_star", "ng-options" => "hotel_star for hotel_star in hotel_stars"}
            .form-group
              %label.control-label.col-xs-2{:for => "inputCountry"} Country
              .col-xs-5
                %input#inputCountry.form-control{"ng-model" => "country", :placeholder => "Country", :type => "text"}
            .form-group
              %label.control-label.col-xs-2{:for => "inputCity"} City
              .col-xs-5
                %input#inputCity.form-control{"ng-model" => "city", :placeholder => "City", :type => "text"}
            .form-group
              %label.control-label.col-xs-2{:for => "inputAddress"} Address
              .col-xs-5
                %input#inputAddress.form-control{"ng-model" => "address", :placeholder => "Address", :type => "text"}
            .form-group
              %label.control-label.col-xs-2{:for => "inputPhone"} Phone
              .col-xs-5
                %input#inputPhone.form-control{"ng-model" => "phone", :placeholder => "Phone", :type => "text"}
            .form-group
              %label.control-label.col-xs-2{:for => "inputImage"} Image
              .col-xs-5
                %input#inputImage.form-control{"file-model" => "myImage", :name => "inputImage", :type => "file"}
          %p
              Image preview:
              %image{"ng-src" => "myImage", "ng-show"=>"ngImage"}
            .form-group
              .col-xs-offset-2.col-xs-10
                %button.btn.btn-primary{"ng-click" => "saveItem(hotel)", :type => "submit"} {{change}}
      %table.table.table-striped{"st-table" => "displayedHotels", "st-safe-src" => "hotels"}
        %thead
          %tr
            %th{"st-sort" => "hotel_name"} hotel_name
            %th{"st-sort" => "hotel_star"} hotel_star
            %th{"st-sort" => "country"} country
            %th{"st-sort" => "city"} city
            %th{"st-sort" => "address"} address
            %th{"st-sort" => "phone"} phone
            %th image     
          %tr
            %th{:colspan => "5"}
              %input.form-control{:placeholder => "global search ...", "st-search" => "", :type => "text"}/
        %tbody
          %tr{"ng-repeat" => "hotel in displayedHotels"}
            %td {{hotel.hotel_name}}
            %td {{hotel.hotel_star}}
            %td {{hotel.country}}
            %td {{hotel.city}}
            %td {{hotel.address}}
            %td {{hotel.phone}}
            %td 
              %img{"ng-src" => "#{$request->{uri_base}}/images/{{hotel.image}}", "ng-show" => "hotel.image" }
            %td
              %button.btn.btn-sm.btn-primary{"ng-click" => "editItem(hotel)", :type => "button"}
                %i.glyphicon.glyphicon-pencil
            %td
              %button.btn.btn-sm.btn-danger{"ng-click" => "removeItem(hotel)", :type => "button"}
                %i.glyphicon.glyphicon-remove-circle
