drop database if exists hotels;

create database hotels
    DEFAULT CHARACTER SET = utf8
    COLLATE = utf8_general_ci;

use hotels;

create table hotel (
  hotel_id int(11) not null auto_increment primary key,
  hotel_name varchar(50) NOT NULL,
  hotel_star enum('Tourist', 'Standard', 'Comfort', 'First Class', 'Luxury') NOT NULL DEFAULT 'Standard',
  -- hotel_star enum(1, 2, 3, 4, 5) NOT NULL DEFAULT 1,
  country varchar(50) NOT NULL,
  city varchar(100) NOT NULL,
  address varchar(100) NOT NULL,
  phone varchar(20) NOT NULL,
  image varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;

insert into hotel (hotel_name, hotel_star, country, city, address, phone)
values 
  ('Amaks',         'Comfort', 'Российская федерация', 'Азов',            'Петровская площадь, 14',   '+7 (86342) 4-12-70'),
  ('Scher Hof',     'Luxury', 'Российская федерация', 'Азов',            'Пляжный проезд, 18',       '+7 (86342) 74-00-2'),
  ('Седьмое небо',  'Luxury', 'Российская федерация', 'Ростов-на-Дону',  'пр. Малиновского, 17',     '+7 (863) 290-06-00'),
  ('Верона',        'First Class', 'Российская федерация', 'Ростов-на-Дону',  'Каскадная, 148',           '+7 (863) 229-57-73')
;

