use utf8;
package Hotels::Schema::Result::Hotel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Hotels::Schema::Result::Hotel

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<hotel>

=cut

__PACKAGE__->table("hotel");

=head1 ACCESSORS

=head2 hotel_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 hotel_name

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 hotel_star

  data_type: 'enum'
  default_value: 'Standard'
  extra: {list => ["Tourist","Standard","Comfort","First Class","Luxury"]}
  is_nullable: 0

=head2 country

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 city

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 address

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 phone

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 image

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=cut

__PACKAGE__->add_columns(
  "hotel_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "hotel_name",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "hotel_star",
  {
    data_type => "enum",
    default_value => "Standard",
    extra => {
      list => ["Tourist", "Standard", "Comfort", "First Class", "Luxury"],
    },
    is_nullable => 0,
  },
  "country",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "city",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "address",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "phone",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "image",
  { data_type => "varchar", is_nullable => 1, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</hotel_id>

=back

=cut

__PACKAGE__->set_primary_key("hotel_id");


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2014-12-21 15:46:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YbAtfyioHd7XZtio462v+Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
